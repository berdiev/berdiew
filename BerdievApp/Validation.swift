//
//  Validation.swift
//  BerdievApp
//
//  Created by Admin on 19/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class Validation {
    
    func validEmail(email: String) -> Bool{
        let regularEx = "[A-Z0-9a-z._%+-]+@[A-XZa-z0-9._]+\\.[A-Za-z]{2,3}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", regularEx)
        return emailTest.evaluate(with: email)
    }
    
    func validPassword(password: String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{6}")
        return passwordTest.evaluate(with: password)
    }
    
    func validPhone(no: String) -> Bool {
      let phoneRegEx = "[0-9]{10}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluate(with: no)
        
    }
}
