//
//  ApplicationViewController.swift
//  BerdievApp
//
//  Created by Admin on 04/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ApplicationViewController: UIViewController {
    
    @IBOutlet weak var nextBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextBtn.cornerRadius = nextBtn.frame.height/2
        nextBtn.addShadow(offset: CGSize.init(width: 2, height: 3), color: .gray, radius: 3, opacity: 0.5)
        
    }
    @IBAction func homeTapped(_ sender: Any) {
        let homeVC = self.storyboard?.instantiateViewController(identifier: "TabBarViewController") as! TabBarViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
        
    }
}
