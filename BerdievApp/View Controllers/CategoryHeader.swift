//
//  CategoryHeader.swift
//  BerdievApp
//
//  Created by Admin on 14/04/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class CategoryHeader: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var viewAllBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewAllBtn.addShadow(offset: CGSize.init(width: 1, height: 1), color: .purple, radius: 1, opacity: 0.4)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
