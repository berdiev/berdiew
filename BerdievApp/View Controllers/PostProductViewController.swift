//
//  PostProductViewController.swift
//  BerdievApp
//
//  Created by Admin on 29/04/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class PostProductViewController: UIViewController {
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var categoryField: UIButton!
    @IBOutlet weak var addBtn: UIButton!
    let transparentView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionTextView.text = "Add descritpion"
        descriptionTextView.textColor = UIColor.lightGray
        descriptionTextView.layer.cornerRadius = 12
        
        addBtn.cornerRadius = addBtn.frame.height/2
        addBtn.addShadow(offset: CGSize.init(width: 2, height: 3), color: .gray, radius: 3, opacity: 0.5)
    }

@IBAction func addProductTapped(_ sender: Any) {
    let addproductVC = self.storyboard?.instantiateViewController(identifier: "AddedProductViewController") as! AddedProductViewController
    self.navigationController?.pushViewController(addproductVC, animated: true)
}

}





