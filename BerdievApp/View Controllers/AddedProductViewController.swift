//
//  AddedProductViewController.swift
//  BerdievApp
//
//  Created by Admin on 01/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AddedProductViewController: UIViewController {

    @IBOutlet weak var markBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        markBtn.cornerRadius = markBtn.frame.height/2
        markBtn.addShadow(offset: CGSize.init(width: 2, height: 3), color: .gray, radius: 3, opacity: 0.5)
       
    }
    

   

}
