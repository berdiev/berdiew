//
//  SectionHeader.swift
//  BerdievApp
//
//  Created by Admin on 14/04/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionViewCell {
    
    @IBOutlet weak var headerlabel: UILabel!
    @IBOutlet weak var headerButton: UIButton!
}
