//
//  SellerProductCollectionViewCell.swift
//  BerdievApp
//
//  Created by Admin on 07/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class SellerProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productweight: UILabel!
    @IBOutlet weak var product: UILabel!
}
