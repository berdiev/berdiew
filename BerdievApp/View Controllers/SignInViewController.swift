//
//  SignInViewController.swift
//  BerdievApp
//
//  Created by Admin on 25/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var SignInGoogle: UIButton!
    @IBOutlet var EmailAddress: UITextField!
    @IBOutlet var Password: UITextField!
    @IBOutlet var SignIn: UIButton!
    @IBOutlet var CreateAccount: UIButton!
    
    override func viewDidLoad(){
        EmailAddress.delegate = self
        Password.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupView()
    }
    //MARK: Delegate method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == EmailAddress {
            textField.resignFirstResponder()
        } else if textField == Password {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func setupView(){
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        SignInGoogle.layer.cornerRadius = SignInGoogle.frame.height/2
        SignInGoogle.backgroundColor = UIColor(red: 248.0/255, green: 248.0/255, blue: 248.0/255, alpha: 1.0)
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            let emailPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 70, height: self.EmailAddress.frame.height))
            EmailAddress.leftView = emailPaddingView
            EmailAddress.leftViewMode = .always
            
            let passwordPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 70, height: self.Password.frame.height))
            Password.leftView = passwordPaddingView
            Password.leftViewMode = .always
        }else{
            let emailPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: self.EmailAddress.frame.height))
            EmailAddress.leftView = emailPaddingView
            EmailAddress.leftViewMode = .always
            
            let passwordPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: self.Password.frame.height))
            Password.leftView = passwordPaddingView
            Password.leftViewMode = .always
        }
        
        SignIn.layer.cornerRadius = SignIn.frame.height/2
        SignIn.addShadow(offset: CGSize.init(width: 2, height: 3), color: .gray, radius: 3, opacity: 0.5)
        
        CreateAccount.layer.cornerRadius = CreateAccount.frame.height/2
        CreateAccount.layer.borderWidth = 1
        CreateAccount.addShadow(offset: CGSize.init(width: 2, height: 3), color: .gray, radius: 3, opacity: 0.5)
    }
    @IBAction func resetPwdTapped(_ sender: Any) {
        let resetPwdVC = self.storyboard?.instantiateViewController(identifier: "ResetPasswordViewController") as! ResetPasswordViewController
        resetPwdVC.modalPresentationStyle = .overCurrentContext
        resetPwdVC.modalTransitionStyle = .crossDissolve
        present(resetPwdVC,animated: true, completion: nil)
    }
    @IBAction func signInButton(_ sender: Any) {
        if EmailAddress.text?.isEmpty == true {
            showInvalidInputAlert(EmailAddress.placeholder!)
        }
            
        else {
            if Password.text?.isEmpty == true {
                showInvalidInputAlert(Password.placeholder!)
            }
            
        }
    }
    func showInvalidInputAlert(_ fieldName : String) {
        UIAlertController.showInfoAlertWithTitle("Alert", message: String(format:"Incorrect email or password.",fieldName), buttonTitle: "Ok")
    }
    
    @IBAction func createAccountButton(_ sender: Any) {
        let createprofileVC = self.storyboard?.instantiateViewController(identifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
        self.navigationController?.pushViewController(createprofileVC, animated: true)
    }
}
