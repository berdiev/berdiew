//
//  AddProductViewController.swift
//  BerdievApp
//
//  Created by Admin on 23/04/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AddProductViewController: UIViewController ,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var firstPhoto: UIImageView!
    @IBOutlet weak var secondPhoto: UIImageView!
    @IBOutlet weak var thirdPhoto: UIImageView!
    @IBOutlet weak var fourthPhoto: UIImageView!
    @IBOutlet weak var fifthPhoto: UIImageView!
    @IBOutlet weak var sixthPhoto: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
    let imagePicker = UIImagePickerController()
    var tapGesture = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextBtn.cornerRadius = nextBtn.frame.height/2
        nextBtn.addShadow(offset: CGSize.init(width: 2, height: 3), color: .gray, radius: 3, opacity: 0.5)
        imagePicker.delegate = self
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.addPhoto))
        self.firstPhoto.addGestureRecognizer(tapGesture)
        self.firstPhoto.isUserInteractionEnabled = true
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.addPhoto))
        self.secondPhoto.addGestureRecognizer(tapGesture)
        self.secondPhoto.isUserInteractionEnabled = true
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.addPhoto))
        self.thirdPhoto.addGestureRecognizer(tapGesture)
        self.thirdPhoto.isUserInteractionEnabled = true
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.addPhoto))
        self.fourthPhoto.addGestureRecognizer(tapGesture)
        self.fourthPhoto.isUserInteractionEnabled = true
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.addPhoto))
        self.fifthPhoto.addGestureRecognizer(tapGesture)
        self.fifthPhoto.isUserInteractionEnabled = true
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.addPhoto))
        self.sixthPhoto.addGestureRecognizer(tapGesture)
        self.sixthPhoto.isUserInteractionEnabled = true
    }
    
    @objc func addPhoto(){
        let actionsheet = UIAlertController(title: "Photo Source", message: "Choose A Source", preferredStyle: .actionSheet)
        actionsheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.sourceType = .camera
                self.present(self.imagePicker, animated: true, completion: nil)
            }else
            {
                print("Camera is Not Available")
            }
        }))
        actionsheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction)in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        actionsheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action:UIAlertAction)in
            self.firstPhoto.image = nil
            self.dismiss(animated: true, completion: nil)
        }))
        actionsheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = actionsheet.popoverPresentationController {
            popoverController.sourceView = self.view
        }
        self.present(actionsheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedPhoto = info[.originalImage] as? UIImage {
            self.firstPhoto.image = selectedPhoto
            self.secondPhoto.image = selectedPhoto
            self.thirdPhoto.image = selectedPhoto
            self.fourthPhoto.image = selectedPhoto
            self.fifthPhoto.image = selectedPhoto
            self.sixthPhoto.image = selectedPhoto     
        }
      
        dismiss(animated: true, completion: nil)
    }
    @IBAction func nextTapped(_ sender: Any) {
        let productVC = self.storyboard?.instantiateViewController(identifier: "PostProductViewController") as! PostProductViewController
        self.navigationController?.pushViewController(productVC, animated: true)
    }
    
}

