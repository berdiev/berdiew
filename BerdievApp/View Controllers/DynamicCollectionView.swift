//
//  DynamicCollectionView.swift
//  BerdievApp
//
//  Created by Admin on 03/04/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class DynamicCollectionView: UICollectionView {

   override func layoutSubviews() {
        super.layoutSubviews()
        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }

}
