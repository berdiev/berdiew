//
//  LocationViewController.swift
//  BerdievApp
//
//  Created by Admin on 20/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

protocol DataPassing {
    func passData(_: String)
    
}

import UIKit

class LocationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var delegate: DataPassing? = nil
    @IBOutlet weak var tableView: UITableView!
    
    var countriesArray = ["Delhi","Chandigarh","Mumbai","Gurugram","Chattisgarh","Mohali","Gujarat","Pune","Bangalore","Ludhiana","Pondicherry"]
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countriesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = countriesArray[indexPath.row]
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
        
        
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    
}

