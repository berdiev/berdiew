//
//  ProfileDetailsViewController.swift
//  BerdievApp
//
//  Created by Admin on 26/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ProfileDetailsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var userImgBtn: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    let imagepicker = UIImagePickerController()
    //var tapGesture = UITapGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagepicker.delegate = self
        nameTextField.delegate = self
        emailTextField.delegate = self
        phoneTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
        //        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.addPhoto))
        //        self.userImageView.addGestureRecognizer(tapGesture)
        //        self.userImageView.isUserInteractionEnabled = true
        
        userImgBtn.layer.shadowOpacity = 15
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            textField.resignFirstResponder()
        } else if textField == emailTextField {
            textField.resignFirstResponder()
        }
        else if textField == phoneTextField {
            textField.resignFirstResponder()
        }
        else if textField == passwordTextField {
            textField.resignFirstResponder()
        }
        else if textField == confirmPasswordTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    @IBAction func addPhoto(_ sender: UIButton) {
        let imagepicker = UIImagePickerController()
        imagepicker.delegate = self
        let alertController = UIAlertController(title: "Photo Source", message: "Choose A Source", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagepicker.sourceType = .camera
                self.present(imagepicker, animated: true, completion: nil)
            }else
            {
                print("Camera is Not Available")
            }
        }))
        alertController.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction)in
            imagepicker.sourceType = .photoLibrary
            self.present(imagepicker, animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action:UIAlertAction)in
            self.userImageView.image = #imageLiteral(resourceName: "userimg")
            self.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = sender.frame
            popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
        self.present(alertController, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedPhoto = info[.originalImage] as? UIImage {
            self.userImageView.image = selectedPhoto
        }
        dismiss(animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        nextButton.cornerRadius = nextButton.frame.height/2
        nextButton.addShadow(offset: CGSize.init(width: 2, height: 3), color: .black, radius: 3, opacity: 0.5)
        setupView()
    }
    func setupView(){
        userImageView.layer.cornerRadius = userImageView.frame.height/2
        let rightbarButton = UIBarButtonItem(image: UIImage.init(named: "3dotMenu"), style: .plain, target: nil, action: #selector(addSideMenu))
        navigationItem.rightBarButtonItem = rightbarButton
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor(red: 0.0/255, green: 133.0/255, blue: 211.0/255, alpha: 1.0)
    }
    @objc func addSideMenu(){
        let view = UIView()
    }
    
    @IBAction func nextTapped(_ sender: Any) {
      //  if nameTextField.text?.isEmpty == true{
          //  showInvalidInputAlert(nameTextField.placeholder!)
            
      //  }
        
        let nextVC = self.storyboard?.instantiateViewController(identifier: "CompanyInfoViewController") as! CompanyInfoViewController
        self.navigationController?.pushViewController(nextVC, animated: true)
        
        
    }
    func showInvalidInputAlert(_ fieldName : String) {
           UIAlertController.showInfoAlertWithTitle("Alert", message: String(format:"Please enter a valid %@.",fieldName), buttonTitle: "OK")
       }
    
}
