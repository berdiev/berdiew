//
//  CommonMethods.swift
//  Otic
//
//  Created by admin on 17/12/19.
//  Copyright © 2019 Unyscape. All rights reserved.
//
import Alamofire         
import Foundation
import UIKit
import MBProgressHUD
import Reachability


class CommonMethods{

    static func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
            "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
        "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    static func validatePassWord(password: String) -> Bool
    {
        let regularExpression = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{7,}"
        
        let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        
        return passwordValidation.evaluate(with: password)
    }
    
}


//MARK:Keyboardhide
extension UIViewController {
//     func deleteAccountAPI(){
//        if Reachability.isConnectedToNetwork() {
//            let parameters : [String:String] = [
//                "user_id":UserDefaults.standard.value(forKey: USER_DEFAULTS_KEYS.USER_ID) as! String,
//            ]
////            print(parameters)
//            showProgressOnView(self.view)
//            ServerClass.sharedInstance.sendMultipartRequestToServerWithParameter(urlString: BASE_URL + PROJECT_URL.DELETE_ACCOUNT_URL, sendJson: parameters, successBlock: { (json) in
//                hideAllProgressOnView(self.view)
//                let status = json["status"].stringValue
//                if status  == "ok" {
////                    print(json)
//                    self.showAlert(withTitle: "", withMessage: "Account Deleted")
//                    logoutUser()
//                }
//                else {
//                    hideAllProgressOnView(self.view)
//                    UIAlertController.showInfoAlertWithTitle("Error", message: json["message"].stringValue, buttonTitle: "okay")
//                }
//            }, errorBlock: { (NSError) in
//                hideAllProgressOnView(self.view)
//                UIAlertController.showInfoAlertWithTitle("Alert!", message: kUnexpectedErrorAlertString, buttonTitle: "okay")
//            })
//        }
//        else{
//            hideAllProgressOnView(self.view)
//            UIAlertController.showInfoAlertWithTitle("Alert!", message: "Kontrollera internetanslutning", buttonTitle: "okay")
//        }
//    }
//    func logOutshowAlert(withTitle title: String, withMessage message:String) {
//        let isLoggedIn:Bool = false
//        UserDefaults.standard.set(isLoggedIn, forKey: USER_DEFAULTS_KEYS.IS_LOGIN)
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        let ok = UIAlertAction(title: "Log Out", style: .default, handler: { action in
//            logoutUser()
//        })
//        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
//            return
//        })
//        alert.addAction(ok)
//        alert.addAction(cancel)
//        DispatchQueue.main.async(execute: {
//            self.present(alert, animated: true)
//        })
//    }
    func DeleteShowAlert(withTitle title: String, withMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Delete", style: .default, handler: { action in
//            self.deleteAccountAPI()
        })
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
            return
        })
        ok.setValue(UIColor.red, forKey: "titleTextColor")
        alert.addAction(ok)
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    func showAlert(withTitle title: String, withMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        let _ = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        })
        alert.addAction(ok)
        //alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
extension UIView {
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
}
extension UIViewController {
    func showIndicator(withTitle title: String, and Description:String) {
        let Indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        Indicator.label.text = title
        Indicator.isUserInteractionEnabled = false
        Indicator.detailsLabel.text = Description
        Indicator.show(animated: true)
        Indicator.tintColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        Indicator.contentColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
    }
    func showIndicatorWithProgress(withTitle title: String, and Description:String, progress:Float){
        let Indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        Indicator.progress = progress
        Indicator.label.text = title
        Indicator.isUserInteractionEnabled = false
        Indicator.detailsLabel.text = Description
        Indicator.show(animated: true)
        Indicator.tintColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        Indicator.contentColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
    }
    func hideIndicator() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
  
}


