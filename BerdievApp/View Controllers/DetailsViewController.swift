//
//  DetailsViewController.swift
//  BerdievApp
//
//  Created by Admin on 15/04/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productWeight: UILabel!
    @IBOutlet weak var product: UILabel!
    @IBOutlet weak var contactBtn: UIButton!
    
    var product_imgURL:UIImage!
    var product_name:String = ""
    var product_weight:String = ""
    var product_detail:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productName.text = product_name
        productWeight.text = product_weight
        product.text = product_detail
        productImage.image = product_imgURL
        
        contactBtn.cornerRadius = contactBtn.frame.height/2
        contactBtn.addShadow(offset: CGSize.init(width: 2, height: 3), color: .gray, radius: 3, opacity: 0.5)
}
    
    @IBAction func contactTapped(_ sender: Any) {
        let contactVC = self.storyboard?.instantiateViewController(identifier: "ContactViewController") as! ContactViewController
        self.navigationController?.pushViewController(contactVC, animated: true)
    }
}
