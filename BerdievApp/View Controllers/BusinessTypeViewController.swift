//
//  BusinessTypeViewController.swift
//  BerdievApp
//
//  Created by Admin on 20/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class BusinessTypeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var tableView1: UITableView!
    var businessTypeArray = ["1","2","3","4","5","6","7","8","9","10"]
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return businessTypeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
        cell1.textLabel?.text = businessTypeArray[indexPath.row]
        return cell1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       self.dismiss(animated: true, completion: nil)
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    

    

}
