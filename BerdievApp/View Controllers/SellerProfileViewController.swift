//
//  SellerProfileViewController.swift
//  BerdievApp
//
//  Created by Admin on 06/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class SellerProfileViewController: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var sendBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    func setupView(){
        profileImage.cornerRadius = profileImage.frame.height/2
        sendBtn.cornerRadius = sendBtn.frame.height/2
        sendBtn.addShadow(offset: CGSize.init(width: 2, height: 3), color: .gray, radius: 3, opacity: 0.5)
           let rightbarButton = UIBarButtonItem(image: UIImage.init(named: "3dotMenu"), style: .plain, target: nil, action: #selector(addSideMenu))
           navigationItem.rightBarButtonItem = rightbarButton
           self.navigationController?.navigationBar.isHidden = false
           self.navigationController?.navigationBar.tintColor = UIColor(red: 0.0/255, green: 133.0/255, blue: 211.0/255, alpha: 1.0)
       }
       @objc func addSideMenu(){
           let view = UIView()
       }
    @IBAction func infoTapped(_ sender: Any) {
        let companyInfoVC = self.storyboard?.instantiateViewController(identifier: "ProfileInfoViewController") as! ProfileInfoViewController
        self.navigationController?.pushViewController(companyInfoVC, animated: true)
    }
    
}
extension SellerProfileViewController: UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SellerProductCollectionViewCell
       // cell.image.image = #imageLiteral(resourceName: "paper")
      //  cell.productName.label = 
        return cell
    }
    
    
}



