//
//  ConfirmEmailViewController.swift
//  BerdievApp
//
//  Created by Admin on 03/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ConfirmEmailViewController: UIViewController {

    @IBOutlet weak var finsihBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        setupView()
        
    }
    func setupView(){
        finsihBtn.cornerRadius = finsihBtn.frame.height/2
        finsihBtn.addShadow(offset: CGSize.init(width: 2, height: 3), color: .gray, radius: 3, opacity: 0.5)
        let rightbarButton = UIBarButtonItem(image: UIImage.init(named: "3dotMenu"), style: .plain, target: nil, action: #selector(addSideMenu))
        navigationItem.rightBarButtonItem = rightbarButton
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor(red: 0.0/255, green: 133.0/255, blue: 211.0/255, alpha: 1.0)
    }
    @objc func addSideMenu(){
        _ = UIView()
       }

    @IBAction func finishTapped(_ sender: Any) {
        let applicationVC = self.storyboard?.instantiateViewController(identifier: "ApplicationViewController") as! ApplicationViewController
        self.navigationController?.pushViewController(applicationVC, animated: true)
    }
}
