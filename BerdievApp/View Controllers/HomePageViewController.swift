//
//  HomePageViewController.swift
//  BerdievApp
//
//  Created by Admin on 05/03/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class HomePageViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,CelltapDelegate {
    
    
    
    @IBOutlet weak var CategoryTableView: UITableView!
    @IBOutlet weak var CategoriesCollectionView: UICollectionView!
    @IBOutlet weak var searchTextField: UITextField!
    
    var productnameArr = ["Plastic","Paper","Metal","Charcoal","Teflon"]
    var productweightArr = ["0.78 kg","0.78 kg","0.78 kg","0.78 kg","0.78 kg"]
    var productdetailArr = ["Republic of Korea","Republic of Korea","Republic of Korea","Republic of Korea","Republic of Korea"]
    var productimgArr = [#imageLiteral(resourceName: "metal"),#imageLiteral(resourceName: "metal"),#imageLiteral(resourceName: "metal"),#imageLiteral(resourceName: "metal"),#imageLiteral(resourceName: "metal")]
    
    override func viewDidLoad() {
        CategoryTableView.register(UINib(nibName: "CategoryHeader", bundle: nil), forCellReuseIdentifier: "CategoryHeader")
        CategoryTableView.delegate = self
        CategoryTableView.dataSource = self
        setupView()
    }
    
    //MARK: DID_PRESS_CELL_DELEGATE
    func didpressCell(section: Int, row: Int) {
        let vc = self.storyboard?.instantiateViewController(identifier: "DetailsViewController") as! DetailsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        vc.product_name = productnameArr[section]
        vc.product_weight = productweightArr[section]
        vc.product_detail = productdetailArr[section]
        vc.product_imgURL = productimgArr[section]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        275
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesTableViewCell", for: indexPath) as! CategoriesTableViewCell
        cell.collectionView.tag = indexPath.section
        cell.collectionView.reloadData()
        cell.delegate = self
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView:CategoryHeader = tableView.dequeueReusableCell(withIdentifier: "CategoryHeader") as! CategoryHeader
        //headerView.seeAllBTN.addTarget(self, action: #selector(viewAllBtnAction(sender:)), for: .touchUpInside)
        headerView.viewAllBtn.tag = section
        headerView.headerLabel.text = "Category's Name"
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = CategoriesCollectionView.dequeueReusableCell(withReuseIdentifier: "Category", for: indexPath) as! CategoriesCollectionViewCell
        //Take from API
        cell.categoriesLabel.text = "Category"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor =  UIColor.gray.cgColor
        cell?.layer.borderWidth = 2
    }
    
    func setupView(){
        if UIDevice.current.userInterfaceIdiom == .pad{
            let searchPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: self.searchTextField.frame.height))
            searchTextField.leftView = searchPaddingView
            searchTextField.leftViewMode = .always
            
        } else {
            let searchPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: self.searchTextField.frame.height))
            searchTextField.leftView = searchPaddingView
            searchTextField.leftViewMode = .always
            
        }
        
        func searchTapped(_ sender: Any) {
            self.performSegue(withIdentifier: "DetailSegue", sender: self)
            
        }
        
        
    }
}
