//
//  CompanyInfoViewController.swift
//  BerdievApp
//
//  Created by Admin on 28/02/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import  CoreLocation
import DropDown
class CompanyInfoViewController: UIViewController, UITextFieldDelegate,UITextViewDelegate {
    
    
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var businessTypeBtn: UIButton!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var createBtn: UIButton!
    
    override func viewDidLoad() {
        companyTextField.delegate = self
        descriptionTextView.delegate = self
        self.hideKeyboardWhenTappedAround()
        
        //MARK: Alignment of textview's placeholder 
        descriptionTextView.text = "Busicess Descritpion"
        descriptionTextView.textColor = UIColor.lightGray
        descriptionTextView.layer.cornerRadius = 12
        
        createBtn.cornerRadius = createBtn.frame.height/2
        createBtn.addShadow(offset: CGSize.init(width: 2, height: 3), color: .black, radius: 3, opacity: 0.5)
        
        
        //
        //        // The view to which the drop down will appear on
        //        dropDown?.anchorView = dropDownbtn // UIView or UIBarButtonItem
        //
        //        // The list of items to display. Can be changed dynamically
        //        dropDown?.dataSource = ["Delhi", "Noida", "Gurugram"]
        //        dropDownbtn.addTarget(self, action: #selector(addDropdownBtn(_:)), for: .touchUpInside)
        //        dropDown?.selectionAction = { [unowned self] (index: Int, item: String) in
        //            self.dropDownbtn.setTitle(item, for: .normal)
        //        }
        
        
        
    }
    
    @IBAction func businessTypeBtnTapped(_ sender: Any) {
        let BusinessTypeVC = self.storyboard?.instantiateViewController(identifier: "BusinessTypeViewController") as! BusinessTypeViewController
        self.present(BusinessTypeVC, animated: true, completion: nil)
        
        
    }
    
    @IBAction func locationBtnTapped(_ sender: Any) {
        let locationVC = self.storyboard?.instantiateViewController(identifier: "LocationViewController") as! LocationViewController
        self.present(locationVC, animated: true, completion: nil)
        
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descriptionTextView.textColor == UIColor.lightGray {
            descriptionTextView.text = nil
            descriptionTextView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if descriptionTextView.text.isEmpty {
            descriptionTextView.text = "Busicess Descritpion"
            descriptionTextView.textColor = UIColor.lightGray
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == companyTextField {
            textField.resignFirstResponder()
        } //else if textField == locationTextField {
        //textField.resignFirstResponder()
        //  }
        
        return true
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            //MARK: Location updates are not authorized.
            manager.stopUpdatingLocation()
            return
        }
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
            print("Get location =(locValue.latitude) (locValue.longitude) ")
            manager.stopUpdatingLocation()
            
        }
    }
    
    @IBAction func checkBoxTapped(_ sender: Any) {
        if (btnCheckBox.isSelected == false)
        {
            btnCheckBox.setBackgroundImage(UIImage(named: "checked"), for: UIControl.State.normal)
            btnCheckBox.isSelected = true;
        }
        else
        {
            btnCheckBox.setBackgroundImage(UIImage(named: "unchecked"), for: UIControl.State.normal)
            btnCheckBox.isSelected = false;
        }
    }
    
    @IBAction func termsTapped(_ sender: Any) {
        let termsVC = self.storyboard?.instantiateViewController(identifier: "TermsViewController") as! TermsViewController
        self.navigationController?.pushViewController(termsVC, animated: true)
        
    }
    @IBAction func createAccount(_ sender: Any) {
        let createaccountVC = self.storyboard?.instantiateViewController(identifier: "ConfirmEmailViewController") as! ConfirmEmailViewController
        self.navigationController?.pushViewController(createaccountVC, animated: true)
    }
    
    
    
}
