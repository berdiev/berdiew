//
//  ProfileViewController.swift
//  BerdievApp
//
//  Created by Admin on 11/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var userProfile: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        userProfile.cornerRadius = userProfile.frame.height/2
        setupView()
    }
    
    func setupView(){
        let rightbarButton = UIBarButtonItem(image: UIImage.init(named: "3dotMenu"), style: .plain, target: nil, action: #selector(addSideMenu))
        navigationItem.rightBarButtonItem = rightbarButton
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor(red: 0.0/255, green: 133.0/255, blue: 211.0/255, alpha: 1.0)
    }
    
    @objc func addSideMenu(){
        let view = UIView()
    }
    @IBAction func profileDetails(_ sender: Any) {
        let profileVC = self.storyboard?.instantiateViewController(identifier: "ProfileInfoViewController") as! ProfileInfoViewController
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @IBAction func companyInfo(_ sender: Any) {
        let companyVC = self.storyboard?.instantiateViewController(identifier: "CompanyViewController") as! CompanyViewController
        self.navigationController?.pushViewController(companyVC, animated: true)
    }
    @IBAction func terms(_ sender: Any) {
        let termsVC = self.storyboard?.instantiateViewController(identifier: "TermsViewController") as! TermsViewController
        self.navigationController?.pushViewController(termsVC, animated: true)
        
    }
    @IBAction func contactBtn(_ sender: Any) {
        let contactTeamVC = self.storyboard?.instantiateViewController(identifier: "ContactTeamViewController") as! ContactTeamViewController
        contactTeamVC.modalPresentationStyle = .overCurrentContext
        contactTeamVC.modalTransitionStyle = .crossDissolve
        present(contactTeamVC, animated: true, completion: nil)
        
    }
    
}
