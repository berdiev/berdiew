//
//  ContactViewController.swift
//  BerdievApp
//
//  Created by Admin on 05/05/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {
    
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var msgTextfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        sendBtn.cornerRadius = sendBtn.frame.height/2
        setupView()
       
    }
    func setupView(){
//        let rightbar = UIImage(named: "userimg")?.withRenderingMode(.alwaysOriginal)
//        let rightbarbutton = UIBarButtonItem(image: rightbar, style: .plain, target: nil, action: nil)
//        navigationItem.rightBarButtonItem = rightbarbutton
//        self.navigationController?.navigationBar.isHidden = false
//        self.navigationController?.navigationBar.maskToBounds = true
//        self.navigationController?.navigationBar.layer.cornerRadius = 12
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            let msgPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: self.msgTextfield.frame.height))
            msgTextfield.leftView = msgPaddingView
            msgTextfield.leftViewMode = .always
           
        }else{
            let msgPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: self.msgTextfield.frame.height))
            msgTextfield.leftView = msgPaddingView
            msgTextfield.leftViewMode = .always

        
    }
   
}
    @IBAction func profileTapped(_ sender: Any) {
        let nextVC = self.storyboard?.instantiateViewController(identifier: "SellerProfileViewController") as! SellerProfileViewController
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
}
